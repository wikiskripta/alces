<?php
	/* this is not an entry point */
	if (!defined("ALCES")) { exit("Not a valid entry point."); }

	/* generate a faculty affiliation to the footer in proper language */
	if (in_array($content["lang"], ["cs", "sk"])) {
		$content["faculty"] = "1. lékařská fakulta Univerzity Karlovy";
	} else {
		$content["faculty"] = "1st Faculty of Medicine, Charles University, Czech Republic";
	}

	/* HTML template follows here */
?><!doctype html>
<html lang="<?= $content["lang"] ?>">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $content["title"] ?></title>
	<link rel="stylesheet" media="screen" href="assets/screen.css">
    <link rel="stylesheet" media="print" href="assets/print.css">
</head>
<body>
	<nav>
		<div><strong><?= $content["menu_title"] ?></strong> | <a href="?new"><?= $content["menu_new"] ?></a></div>
		<div>
			<form method="post">
				<select name="lang">
					<option value="cs" <?= ($content["lang"] === "cs" ? "selected" : "") ?>>Česky</option>
					<option value="en" <?= ($content["lang"] === "en" ? "selected" : "") ?>>English</option>
				</select>
				<input type="submit" value="OK">
			</form>
		</div>
	</nav>
	<hr>
	<main>
		<h1><?= $content["title"] ?></h1>
		<?= $content["body"] ?>
	</main>
	<footer>
		<hr>
		<p>&copy; 2020 Petr Kajzar, <?= $content["faculty"] ?>.
		[<?= $content["version"] ?>: <?= ALCES ?>,
		<a href="https://bitbucket.org/wikiskripta/alces"><?= $content["source_code"] ?></a>]
		</p>
	</footer>
</body>
</html>
