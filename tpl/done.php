<?php
/* this is not an entry point */
if (!defined("ALCES")) { exit("Not a valid entry point."); }

/* extract settings for the text */
extract($settings, EXTR_PREFIX_ALL, "set");

/* get a list of generated questions for the text */
$set_exam_sorted = $set_exam;
sort($set_exam_sorted);
$sq = implode(", ", $set_exam_sorted);
$uq = implode(", ", $set_exam);

/* get current timestamp */
$tstmp = date("c", filemtime($filename));

/* get singular/plural */
$qword = $set_count > 1 ? $content["questions_pl"] : $content["questions_sg"];

/* done page HTML body */
$content["body"] = <<<CNT_DONE
<p><strong>{$content["done_picked"]}</strong></p>
<p>{$content["done_text"]}</p>
<blockquote><span>$sq</span></blockquote>
<p>{$content["done_details"]} $set_count $qword {$content["from"]} $set_min
{$content["to"]} $set_max.</p>
<p>{$content["unsorted"]}: $uq.</p>
<pre>[ID $hash, $tstmp]</pre>
CNT_DONE;

/* basic HTML template */
include "template.php";
