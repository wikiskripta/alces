<?php
/* this is not an entry point */
if (!defined("ALCES")) { exit("Not a valid entry point."); }

/* extract poll settings for the text */
extract($settings, EXTR_PREFIX_ALL, "set");

/* get singular/plural */
$qword = $set_count > 1 ? $content["questions_pl"] : $content["questions_sg"];

/*some additional student info (policy) */
$content["additional_student_info"] = "";
if (isset($config["student_policy_link"])) {
    if (is_array($config["student_policy_link"])) {
        if (isset($config["student_policy_link"][$content["lang"]])) {
            $content["additional_student_info"] = str_replace("$1", $config["student_policy_link"][$content["lang"]], $content["student_policy"]);
        }
    } else {
        $content["additional_student_info"] = str_replace("$1", $config["student_policy_link"], $content["student_policy"]);
    }
    $content["additional_student_info"] = "<p>" . $content["additional_student_info"] . "</p>";
}
/* student's page HTML body */
$content["body"] = <<<CNT_STUDENT
<p><strong>{$content["student_welcome"]}</strong></p>
<p>{$content["student_id"]} <code>$hash</code>. {$content["student_pick"]}
$set_count $qword {$content["from"]} $set_min
{$content["to"]} $set_max.</p>
{$content["additional_student_info"]}
<form method="post" action="?exam=$hash">
<button type="submit">{$content["student_button"]}</button>
</form>
CNT_STUDENT;

/* basic HTML template */
include "template.php";
