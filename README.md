Pick random questions for a distance (online) exam
==================================================

A simple PHP application used for picking random questions for distance
examination.

Description
-----------

The teacher generates a link where the student picks questions for his/her
online exam. The teacher then sees the picked questions also in his/her browser
(via the generated link).

Installation
------------

Upload the app to the web directory of your choice. It is ideal to clone
repository directly from git (`git clone`) so that you can continuously update
the application from git (`git pull` / `git fetch`). You can contact the author
for additional help:

	petr.kajzar@lf1.cuni.cz

Settings
--------

The application needs either the right to create the `data` directory where it
stores temporary files. Alternatively, you can create this directory yourself
and set permissions so that the PHP script can save files in it (e.g. use
`chmod`).

More help to be found e.g. here:

	https://stackoverflow.com/a/49566838

In case of problems please contact the author, see e-mail address above.

You can also modify a list of recommended videoconference software to
a new `config.json` file in the root directory of this app. Example follows:

	{
    	"videoconferences": {
        	"Jitsi" : "https://meet.jit.si",
        	"Microsoft Teams" : "https://teams.microsoft.com"
    	}
	}

If your school has some kind of special rules for distance exam, you can point
them if `config.json` as well:

	{
    	"student_policy_link": "https://example.com"
	}

Or language-specific links:

	{
    	"student_policy_link": {
             "cs" : "https://example.com/cs.html",
             "en" : "https://example.com/en.html"
        }
	}

License
-------

© Petr Kajzar, 1st Faculty of Medicine, Charles University, 2020.

The source code is available under the MIT license.
