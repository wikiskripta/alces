<?php

/* inicialization with version number */
const ALCES = "0.2.5";

/* load some configuration, if exists */
if (file_exists("config.json")) {
	$config = json_decode(file_get_contents("config.json"), true);
}

/*
 * Internationalization:
 *  1. if the language has been just set up, get that language
 *  2. if no language chosen in a cookie, get proper language from the HTTP header
 *  3. store that language in a cookie
 *  4. if cookie present, get that cookie settings
 *  5. if the language is not supported, get English messages
 */
if (isset($_POST["lang"])) {
	setcookie("lang", $_POST["lang"]);
	$content["lang"] = $_POST["lang"];
}

if (!isset($_COOKIE["lang"]) && !isset($content["lang"])) {
	$parsed_language = locale_parse(locale_accept_from_http($_SERVER["HTTP_ACCEPT_LANGUAGE"]));
	$content["lang"] = $parsed_language["language"];
	setcookie("lang", $content["lang"]);
}

if (isset($_COOKIE["lang"]) && !isset($content["lang"])) {
	$content["lang"] = $_COOKIE["lang"];
}

$lang_filename = "i18n/" . $content["lang"] . ".php";

if (file_exists($lang_filename)) {
	include $lang_filename;
} else {
	include "i18n/en.php";
}

/* HTTP headers to prevent abuse */
header("X-Content-Type-Options: nosniff");
header("Referrer-Policy: no-referrer");
header("X-Frame-Options: DENY");
header("X-XSS-Protection: 1; mode=block");
header("Feature-Policy: accelerometer 'none'; geolocation 'none';" .
	"midi 'none'; microphone 'none'; camera 'none'; magnetometer 'none';" .
	"gyroscope 'none'; payment 'none';");
header("Content-Security-Policy: default-src 'none'; style-src 'self';" .
	"img-src 'self'; frame-ancestors 'none'; form-action 'self';");

/*
 * This makes link to poll ID for the current role, e.g.:
 *  - http://example.com/?student=9082098209
 *  - https://www.example.com/?teacher=9082098209
 *
 * Function returns complete anchor element in HTML.
 */
function makelink($role, $id) {
	$url = (!isset($_SERVER["HTTPS"]) ? "http" : "https") . "://" . $_SERVER["HTTP_HOST"] .
	str_replace("index.php", "", $_SERVER["SCRIPT_NAME"]) . "?$role=$id";
	return('<a href="' . $url . '">' . $url . '</a>');
}

/*
 * This cleans the data folder.
 * Every file older than $time is deleted.
 */
function delete_files($time) {
	if (!file_exists("data/")) {
		return;
	}
    $files = glob("data/*");
    foreach($files as $file) {
        if(is_file($file) && (filemtime($file) < $time)) {
            unlink($file);
        }
    }
    return;
}

/*
 * Get new ID:
 *  1. validate all parameters (and if fails, display error page)
 *  2. create a unique ID from the current time stamp and some random characters
 *  3. store poll information to a TXT file
 *  4. redirect the user directly to teacher's scope of that ID
*/
if (isset($_GET["new"]) && isset($_GET["min"]) && isset($_GET["max"]) && isset($_GET["new"])) {

	$options = [
		"options" => ["min_range" => 1, "max_range" => 999]
	];

	if ($_GET["min"] >= $_GET["max"]
			|| $_GET["count"] > ($_GET["max"] - $_GET["min"] + 1)
			|| !filter_var($_GET["min"], FILTER_VALIDATE_INT, $options)
			|| !filter_var($_GET["max"], FILTER_VALIDATE_INT, $options)
			|| !filter_var($_GET["count"], FILTER_VALIDATE_INT, $options)) {
		include "tpl/error.php";
		exit();
	}
	$hash = base_convert(rand(10, 35), 10, 36) . base_convert(time(), 10, 36) . base_convert(rand(0, 35), 10, 36);
	unset($_GET["new"]);
	mkdir("data");
	file_put_contents("data/$hash.txt", json_encode($_GET));
	header("Location: ?teacher=$hash");
	exit();
}

/*
 * Generate exam questions:
 *  1. validate the ID (if does not exist, display error page)
 *  2. generate random numbers in the selected range
 *  3. redirect to the student's scope of the page
 */
if (isset($_GET["exam"]) && count($_GET) === 1) {
	$hash = $_GET["exam"];
	$filename = "data/$hash.txt";
	if (!file_exists($filename)) {
		include "tpl/noexist.php";
		exit();
	}
	$settings = json_decode(file_get_contents($filename), true);
	if (!isset($settings["exam"])) {
		$exam = [];
		do {
			$a = rand($settings["min"], $settings["max"]);
			if (array_search($a, $exam) === false) {
				$exam[] = $a;
			}
		} while (count($exam) < $settings["count"]);
		$settings["exam"] = $exam;
		file_put_contents($filename, json_encode($settings));
	}
	header("Location: ?student=$hash");
	exit();
}

/*
 * Teacher's scope:
 *  1. validate GET parameters (and if fails, display error page)
 *  2. if ID does not exist at all, display error page
 *  3. if ID exists and contains generated exam question, display questions
 *  4. if ID is just newly generated, show instructions for teacher
 */
if (isset($_GET["teacher"])  && count($_GET) === 1) {
	$hash = $_GET["teacher"];
	$filename = "data/$hash.txt";
	if (!file_exists($filename)) {
		include "tpl/noexist.php";
		exit();
	}
	$settings = json_decode(file_get_contents("data/" . $_GET["teacher"] . ".txt"), true);
	if (isset($settings["exam"])) {
		include "tpl/done.php";
		exit();
	}
	header("Refresh: 10");
	include "tpl/teacher.php";
	exit();
}

/*
 * Student's scope:
 *  1. validate GET parameters (and if fails, display error page)
 *  2. if ID does not exist at all, display error page
 *  3. if ID exists and contains generated exam question, display questions
 *  4. if ID is just newly generated, show instructions for student
 */
if (isset($_GET["student"])  && count($_GET) === 1) {
	$hash = $_GET["student"];
	$filename = "data/$hash.txt";
	if (!file_exists($filename)) {
		include "tpl/noexist.php";
		exit();
	}
	$settings = json_decode(file_get_contents("data/$hash.txt"), true);
	if (isset($settings["exam"])) {
		include "tpl/done.php";
		exit();
	}
	include "tpl/student.php";
	exit();
}

/* show home page */
include "tpl/home.php";

/* delete files older than 60 days */
delete_files(strtotime("-60 days"));
