<?php

## General strings #############################################################

# main title
$content["title"] = "Losování otázek pro online zkoušení";

# menu title
$content["menu_title"] = "Losování otázek";

# menu: new pick
$content["menu_new"] = "Nové losování";

# questions
$content["questions_sg"] = "otázky";
$content["questions_pl"] = "otázek";

# question range
$content["from"] = "v rozmezí";
$content["to"] = "až";

# app version
$content["version"] = "verze";

# source code
$content["source_code"] = "zdrojový kód";

## Home page ###################################################################

# introduction
$content["home_intro"] = <<<HOMEINTRO
Na této stránce si můžete nechat vylosovat čísla otázek pro online zkoušku.
HOMEINTRO;

# project description
$content["home_description"] = <<<HOMEDESCRIPTION
Zkoušející i student vidí vylosovaná čísla otázek každý na svém počítači.
Zkouška pak může probíhat pomocí některého z videokonferenčních nástrojů
HOMEDESCRIPTION;

# procedure: introduction
$content["home_procedure_intro"] = "Postup je jednoduchý:";

# procedure: 1st step
$content["home_procedure_1"] = <<<HOMEPROCEDURE1
Zkoušející pedagog navštíví tuto stránku a ve formuláři níže zvolí rozsah
zkouškových otázek a počet losovaných otázek z tohoto rozsahu.
HOMEPROCEDURE1;

# procedure: 2nd step
$content["home_procedure_2"] =  <<<HOMEPROCEDURE2
Pedagog pomocí tlačítka nechá vygenerovat odkaz na nové losování podle
zadaných parametrů. Odkaz pošle studentovi e-mailem nebo do chatu
ve videokonferenci.
HOMEPROCEDURE2;

# procedure: 3rd step
$content["home_procedure_3"] = "Student si otevře odkaz a může losovat.";

# procedure: 4th step
$content["home_procedure_4"] = <<<HOMEPROCEDURE4
Jakmile losování otázek ke zkoušce proběhne, pedagog i student uvidí čísla
otázek u sebe v prohlížeči.
HOMEPROCEDURE4;

# form: legend
$content["home_form_legend"] = "Vytvořit nové losování";

# form: question range
$content["home_form_range"] = "Rozsah čísel otázek ke zkoušce:";

# form: number of picked questions
$content["home_form_count"] = "Počet losovaných otázek:";

# form: create a poll
$content["home_form_create"] = "Vytvořit";

## Teacher's scope #############################################################

# teacher's instructions - send url
$content["teacher_instructions_url"] = "Předejte prosím studentovi adresu:";

# teacher's instructions - next steps
$content["teacher_instructions_head"] = "A co dál?";

# teacher's instructions - first step
$content["teacher_instructions_1"] = <<<TEACHERINST1
Pošlete výše uvedený odkaz studentovi a vyzvěte jej, aby si nechal vylosovat
otázky ke zkoušce. Vy ale zůstaňte na této stránce!
TEACHERINST1;

# teacher!s instructions - second step
$content["teacher_instructions_2"] = <<<TEACHERINST2
Jakmile si student otázky vylosuje, na této stránce se vám nejpozději
do 10 sekund ukáže číslo vylosované otázky.
TEACHERINST2;

## Student's scope #############################################################

# student's welcome message
$content["student_welcome"] = "Vítáme vás na zkoušce!";

# student's ID
$content["student_id"] = "Bylo vám přiděleno ID";

# student's pick
$content["student_pick"] = "Čeká vás výběr";

# additional student policy
$content["student_policy"] = <<<STUDENTPOLICY
Pokračováním v losování souhlasíte s <a href="$1">podmínkami distanční zkoušky</a>.
STUDENTPOLICY;

# student's button
$content["student_button"] = "Vylosovat otázky";

## Errors ######################################################################

# not valid parameteres
$content["error_parameters"] = <<<ERRPAR
Zadali jste podivné parametry, <a href="?new">zkuste to prosím znovu</a>.
ERRPAR;

# check parameters
$content["error_check_parameters"] = <<<ERRCHECKPAR
Zkontrolujte, že máte dobře nastavený rozsah otázek a že počet losovaných
otázek nepřesahuje zadaný rozsah.
ERRCHECKPAR;

# ID does not exist
$content["noexist_id"] = "Zadané ID losování neexistuje.";

# not existing ID instructions
$content["noexist_instructions"] = <<<NOEXINSTR
Nejprve si prosím <a href="?new">vytvořte nové losování</a>
a poté postupujte podle pokynů.
NOEXINSTR;

## Done ########################################################################

# questions picked (heading)
$content["done_picked"] = "Vylosováno!";

# questions picked (text)
$content["done_text"] = <<<DONETXT
Pro zkoušku byla vylosována následující čísla zkouškových otázek:
DONETXT;

# questions range
$content["done_details"] = "Byl proveden výběr";

# unsorted questions
$content["unsorted"] = "Otázky byly vylosovány v tomto neseřazeném pořadí";
