<?php

## General strings #############################################################

# main title
$content["title"] = "A pick of questions for online testing";

# menu title
$content["menu_title"] = "Pick questions";

# menu: new pick
$content["menu_new"] = "New pick";

# questions
$content["questions_sg"] = "question";
$content["questions_pl"] = "questions";

# question range
$content["from"] = "from";
$content["to"] = "to";

# app version
$content["version"] = "version";

# source code
$content["source_code"] = "source code";

## Home page ###################################################################

# introduction
$content["home_intro"] = <<<HOMEINTRO
On this page you can pick random questions for your online exam.
HOMEINTRO;

# project description
$content["home_description"] = <<<HOMEDESCRIPTION
Both the examiner and the student can see the picked numbers of questions each
on their computer. The test can then be carried out using
one of the videoconferencing tools
HOMEDESCRIPTION;

# procedure: introduction
$content["home_procedure_intro"] = "The procedure is simple:";

# procedure: 1st step
$content["home_procedure_1"] = <<<HOMEPROCEDURE1
The examiner visits this page and changes the question range and number
of picked questions for the exam.
HOMEPROCEDURE1;

# procedure: 2nd step
$content["home_procedure_2"] = <<<HOMEPROCEDURE2
The examiner generates a link for a new pick according to selected options.
Then he/she sends the link to the student by email or chat.
HOMEPROCEDURE2;

# procedure: 3rd step
$content["home_procedure_3"] = <<<HOMEPROCEDURE3
The student opens the link and can pick a random question.
HOMEPROCEDURE3;

# procedure: 4th step
$content["home_procedure_4"] = <<<HOMEPROCEDURE4
As soon as the exam questions are drawn by the student, both the examiner and
the student will see the picked numbers in their browser.
HOMEPROCEDURE4;

# form: legend
$content["home_form_legend"] = "Create a new poll";

# form: question range
$content["home_form_range"] = "Range of exam questions:";

# form: number of picked questions
$content["home_form_count"] = "Number of picked questions:";

# form: create a poll
$content["home_form_create"] = "Create";

## Teacher's scope #############################################################

# teacher's instructions - send url
$content["teacher_instructions_url"] = "Please send this URL to the student:";

# teacher's instructions - next steps
$content["teacher_instructions_head"] = "And what else?";

# teacher's instructions - first step
$content["teacher_instructions_1"] = <<<TEACHERINST1
Send the link above to the student and ask him/her to draw random exam
questions. But you stay on this page!
TEACHERINST1;

# teacher!s instructions - second step
$content["teacher_instructions_2"] = <<<TEACHERINST2
As soon as the student draws the questions, this page will tell you the
picked random exam questions within 10 seconds.
TEACHERINST2;

## Student's scope #############################################################

# student's welcome message
$content["student_welcome"] = "Welcome to the exam!";

# student's ID
$content["student_id"] = "You have been assigned an ID";

# student's pick
$content["student_pick"] = "Awaiting selection of";

# additional student policy
$content["student_policy"] = <<<STUDENTPOLICY
By continuing you agree to the <a href="$1">terms of distance exam</a>.
STUDENTPOLICY;

# student's button
$content["student_button"] = "Pick questions";

## Errors ######################################################################

# not valid parameteres
$content["error_parameters"] = <<<ERRPAR
You have entered strange parameters, <a href="?new">please try again</a>.
ERRPAR;

# check parameters
$content["error_check_parameters"] = <<<ERRCHECKPAR
Make sure you have a correct range of questions and that the number of picked
questions does not exceed the specified range.
ERRCHECKPAR;

# ID does not exist
$content["noexist_id"] = "The specified ID does not exist.";

# not existing ID instructions
$content["noexist_instructions"] = <<<NOEXINSTR
First, please <a href="?new">create a new poll</a> and then follow
the instructions.
NOEXINSTR;

## Done ########################################################################

# questions picked (heading)
$content["done_picked"] = "Done!";

# questions picked (text)
$content["done_text"] = <<<DONETXT
The following exam question numbers were picked for the exam:
DONETXT;

# questions range
$content["done_details"] = "A selection has been made of";

# unsorted questions
$content["unsorted"] = "Unsorted selection (i.e. original order) of questions";
